CONTENT:
--------
1. Module description
2. Typical usage
3. Installation/configuration

1. Module  description
----------------------

This module creates 'recent content' blocks, similar to the ones provided by
tracker module and comment module, but better ;) It works only for node-types.
You start with one block and can add new block on the fly, with one click.
This module is very simple and small, if you need anything more sophisticated
you might want to check 'views' module.

Each block displays 'recent content' in the form of a list, and can be 
configured:
- which node types are considered (forum, page, books etc)
- whether consider comments (replies) or not
- how many items to display
- block title to display
- sort by what
- what elements to display (title, date, replies)
- custom link
- [common options: on which pages display the block, etc]


2. Typical usage
----------------

Usually I create 2 blocks for the fronpage: 
- one that lists all node types except forum, and does not consider comments. 
  This block lists changes in articles created by site admins, lists real
  content that was recently added or updated
- one block that lists all node types and also considers comments.
  This block is for 'live' content, i.e. what other users read and reply.

I also create one block only for forum pages, that lists only forum type and 
also consider comments. Users can see which forum topics were recently added 
or to which other users recently replied.


3. Installation/configuration
-----------------------------

Enable the module. In administer > recent blocks you can see all your recent 
blocks (one is defined by default), you can add new block, delete existing one 
and configure them.

Remember, that after the block is configured, you have to enable it. You can
do it from normal administer > blocks page, there's also a link to that page 
from recent blocks page.

